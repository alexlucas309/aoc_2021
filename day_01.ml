open! Util

module One = struct
  let solution depths =
    let ans, _ =
      List.fold_left
        (fun (count, prev) e -> (count + if e > prev then 1 else 0), e)
        (0, List.nth depths 0)
        depths in
    ans
  let run' solution =
    let depths = List.map int_of_string @@ lines_of "inputs/input01.txt" in
    solution depths |> string_of_int |> print_endline
  let run () = run' solution
end

module Two = struct
  let rec window_sums = function
    | a :: (b :: c :: _ as inc) -> (a + b + c) :: window_sums inc
    | _ -> []
  let run () = One.run' (One.solution << window_sums)
end
