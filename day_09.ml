open! Util

let grid =
  let rec lsOfNX n x = if n <> 0 then x :: lsOfNX (n - 1) x else [] in
  let colons = lsOfNX 102 ':' in
  lines_of "inputs/input09.txt"
  |> fun y ->
  (colons :: List.map (fun x -> (':' :: String.explode x) @ [ ':' ]) y) @ [ colons ]
  |> List.mapi (fun i x -> List.mapi (fun j y -> (i, j), y) x)

module One = struct
  let rec triplet_windows = function
    | a :: (b :: c :: _ as inc) -> (a, b, c) :: triplet_windows inc
    | _ -> []

  let rec find_lows = function
    | ( _ :: ((_, xb) :: _ :: _ as xi)
      , (_, ya) :: ((co, yb) :: (_, yc) :: _ as yi)
      , _ :: ((_, zb) :: _ :: _ as zi) ) ->
        (if yb < xb && yb < ya && yb < yc && yb < zb then [ co, yb ] else [])
        @ find_lows (xi, yi, zi)
    | _ -> []

  let run () =
    triplet_windows grid |> List.map find_lows |> List.flatten
    |> List.map (fun (_, c) -> int_of_char c - 47)
    |> Ls.sum |> string_of_int |> print_endline
end

module Two = struct
  let grid_arr = grid |> Ls.map Array.of_list |> Array.of_list

  let rec basin_at ((i, j) as p) =
    let get (i, j) = snd grid_arr.(i).(j) in
    let check b = if get p < get b && get b < '9' then basin_at b else [] in
    p :: (check (i + 1, j) @ check (i - 1, j) @ check (i, j + 1) @ check (i, j - 1)) |> Ls.uniq

  let lows = One.triplet_windows grid |> Ls.map One.find_lows |> Ls.flatten |> Ls.map fst

  let run () =
    Ls.(map basin_at lows |> sort (flip (psi ( - ) length)) |> take 3 |> map length |> product)
    |> string_of_int |> print_endline
end
