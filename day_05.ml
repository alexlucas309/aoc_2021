open! Util

type point = float * float
type line = point * point
type int_point = int * int
type int_line = int_point * int_point

let gli_not_colinear (((ax, ay), (bx, by)) : line) (((cx, cy), (dx, dy)) : line) : point option =
  let s1x, s1y, s2x, s2y = bx -. ax, by -. ay, dx -. cx, dy -. cy in
  let s =
    (((0. -. s1y) *. (ax -. cx)) +. (s1x *. (ay -. cy))) /. (0. -. (s2x *. s1y) +. (s1x *. s2y))
  in
  let t = ((s2x *. (ay -. cy)) -. (s2y *. (ax -. cx))) /. (0. -. (s2x *. s1y) +. (s1x *. s2y)) in
  if s >= 0. && s <= 1. && t >= 0. && t <= 1. then Some (ax +. (t *. s1x), ay +. (t *. s1y))
  else None

(* let glis_colinear (((ax, ay), (bx, by)) : line) (((cx, cy), (dx, dy)) : line) : point list option = *)

let endpoints : line list =
  lines_of "inputs/input05.txt"
  |> List.map (fun el ->
         let m = String.split_on_char ' ' el in
         let a = List.nth m 0 |> String.split_on_char ',' in
         let b = List.nth m 2 |> String.split_on_char ',' in
         match a, b with
         | ax :: ay :: _, bx :: by :: _ ->
             (float_of_string ax, float_of_string ay), (float_of_string bx, float_of_string by)
         | _ -> raise (Failure "fuckin' kaboom") )

module One = struct
  let glii u v : int_point option =
    match gli_not_colinear u v with
    | Some (a, b) -> Some (int_of_float a, int_of_float b)
    | _ -> None

  let rel (((ax, ay), (bx, by)) : line) (((cx, cy), (dx, dy)) : line) =
    if ax = bx && bx = cx && cx = dx then `Vert
    else if ay = by && by = cy && cy = dy then `Hori
    else `Perp

  (* let glic (((ax, ay), (bx, by)) as v : line) (((cx, cy), (dx, dy)) as w : line) : (int * int) list option =
     match rel v w with
     | `Hori ->
     | `Vert ->
     | _ -> raise (Failure "oof") *)

  let run () = ()
end

module Two = struct
  let run () = ()
end
