open! Util

let input = lines_of "inputs/input14.txt"

let rec view = function
  | a :: (b :: _ as k) -> (a, b) :: view k
  | _ -> []

let template =
  Ls.hd input |> (String.to_list >> view) |> Ls.group_by id |> Ls.map (bimap_snd Ls.length)

let rules =
  Ls.drop 2 input
  |> Ls.map (fun x ->
         let l = String.split_on_string ~by:" -> " x in
         Ls.hd l, Ls.last l )
  |> Ls.map (bimap (fun s -> s.[0], s.[1]) (fun s -> s.[0]))

module One = struct
  let transform_pair ((((x, y) as proto), c) as whole) =
    match Ls.find_opt (fst >> ( = ) proto) rules with
    | None -> [ whole ]
    | Some (_, m) -> [ (x, m), c; (m, y), c ]

  let transform_poly =
    Ls.(
      map transform_pair >> flatten >> group_by fst
      >> map (bimap_snd (sum_by snd)) )

  let poly_result = applied_n_times_to transform_poly 10 template

  (* The number of characters in the final polymer = 1 + the number of pairs in poly_result.
     So, every character, appears twice — once for each pair it composes,
     except for the first and last character in the polymer, which appear twice + 1.
     The starting and ending characters of the final polymer are the same as those of the template.
     So, we can get the exact frequency of every character in the final polymer. *)

  let freqs poly =
    Ls.(
      map (fun ((x, y), c) -> [ x, c; y, c ]) poly
      |> flatten |> group_by fst
      |> map (bimap_snd (sum_by snd))
      |> map (fun (c, i) ->
             ( c, (i / 2)
               +
               if mem c [ (hd input).[0]; (hd input).[String.length (hd input) - 1] ] then 1 else 0
             ) ) )
  
    let run' freqs =
      let (min, max) = Ls.map snd freqs |> Ls.min_max in
      max - min |> string_of_int |> print_endline
  
    let run () = run' @@ freqs poly_result
  
end

module Two = struct
  let run () = One.(freqs (applied_n_times_to transform_poly 40 template) |> run')
end
