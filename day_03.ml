open Util

module One = struct
  let input = lines_of "inputs/input03.txt"

  let int_from_binary str =
    let result = ref 0 in
    let () =
      for i = String.length str - 1 downto 0 do
        result :=
          !result
          + (int_of_char str.[i] - 48)
            * int_of_float (2. ** float_of_int (String.length str - 1 - i))
      done
    in
    !result

  let solve =
    let bittage = [| 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0 |] in
    let send str =
      for i = 0 to 11 do
        bittage.(i) <- (bittage.(i) + if str.[i] = '0' then -1 else 1)
      done
    in
    let () = List.iter send input in
    let bittage = Array.map (fun x -> if x < 0 then 0 else 1) bittage in
    let gamma_str = Array.fold_left (fun acc elem -> acc ^ string_of_int elem) "" bittage in
    let epsilon_str = String.map (fun x -> if x = '0' then '1' else '0') gamma_str in
    int_from_binary ("0000" ^ gamma_str), int_from_binary ("0000" ^ epsilon_str)

  let run () =
    let g, e = solve in
    print_endline (string_of_int (g * e))
end

module Two = struct
  (* Find both at the same time, by returning a `string * string` from gas and taking a `string list * string list` *)

  let common inc i =
    List.fold_left (fun acc e -> (if e.[i] = '0' then ( - ) else ( + )) acc inc) 0
    >> fun x -> if inc < 0 then if x <= 0 then '0' else '1' else if x < 0 then '0' else '1'

  let gas report common =
    let rec aux i = function
      | [ x ] -> x
      | xs ->
          let c = common i xs in
          aux (i + 1) (List.filter (fun x -> x.[i] = c) xs)
    in
    aux 0 report

  let run () =
    One.int_from_binary ("0000" ^ gas One.input (common 1))
    * One.int_from_binary ("0000" ^ gas One.input (common (-1)))
    |> string_of_int |> print_endline
end