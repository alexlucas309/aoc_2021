open! Util
open Util.Ls

let input =
  lines_of "inputs/input06.txt" |> Ls.hd |> String.split_on_char ',' |> Ls.map Int64.of_string

module One = struct
  let advance = Ls.fold_left (fun ac el -> (if el > 0L then [ el -- 1L ] else [ 6L; 8L ]) @ ac) []
  let run () = applied_n_times_to advance 80 input |> Ls.length |> string_of_int |> print_endline
end

module Two = struct
  let advance =
    let rec aux = function
      | (timer, pop) :: fishes ->
          (if timer = 0L then [ 6L, pop; 8L, pop ] else [ timer -- 1L, pop ]) @ aux fishes
      | _ -> []
    in
    aux >> Ls.(group_by fst >> map (bimap_snd @@ fold_left (fun ac (_, el) -> ac ++ el) 0L))

  let run () =
    Ls.map (fun x -> x, 1L) input
    |> applied_n_times_to advance 256
    |> Ls.fold_left (fun ac (_, el) -> ac ++ el) 0L
    |> Int64.to_string |> print_endline
end
