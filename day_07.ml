open! Util

let input =
  lines_of "inputs/input07.txt" |> Ls.hd |> String.split_on_char ',' |> Ls.map int_of_string

module One = struct
  let run () =
    1 >< 1999
    |> Ls.map (fun i -> Ls.fold_left (fun ac el -> ac + Int.abs (el - i)) 0 input)
    |> Ls.fold_left (fun ac el -> if el < ac then el else ac) Int.max_num
    |> string_of_int |> print_endline
end

module Two = struct
  let run () =
    1 >< 1999
    |> Ls.map (fun i -> Ls.fold_left (fun ac el -> ac + Ls.sum (1 >< Int.abs (el - i))) 0 input)
    |> Ls.fold_left (fun ac el -> if el < ac then el else ac) Int.max_num
    |> string_of_int |> print_endline
end