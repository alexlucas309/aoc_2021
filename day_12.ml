open! Util

let input =
  Ls.map (fun x ->
      let ls = String.split_on_char '-' x in
      Ls.hd ls, Ls.nth ls 1 )
  @@ lines_of "inputs/input12.txt"

module One = struct
  let caves_graph = Hashtbl.create 20
  let () =
    let aux a b = Hashtbl.(replace caves_graph a (find_default caves_graph a [] @ [ b ])) in
    Ls.iter (fun (a, b) -> aux a b ; aux b a) input

  let rec find_paths seen start =
    let seen' = (if Char.is_lowercase start.[0] then [ start ] else []) @ seen in
    let adjacents = Hashtbl.find caves_graph start in
    if Ls.exists (( = ) start) seen then []
    else if start = "end" then [ [ start ] ]
    else Ls.(fold_left (fun ac el -> map (cons start) (find_paths seen' el) @ ac)) [] adjacents

  let run () = find_paths [] "start" |> Ls.length |> string_of_int |> print_endline
end

module Two = struct
  let run () = ()
end
