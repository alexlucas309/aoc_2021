open! Util

let ((cfgs, _) as input) =
  lines_of "inputs/input08.txt"
  |> Ls.unzip_with (fun x ->
         match Str.split (Str.regexp " \\| ") x with
         | x :: y :: _ -> x, y
         | _ -> raise (Failure "shit") )

module One = struct
  let run () = ()
end

module Two = struct
  let run () = ()
end
