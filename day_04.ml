open! Util

let rec extract_board_strings =
  let rec aux = function
    | [] | "" :: _ -> []
    | x :: xs -> x :: aux xs
  in
  function
  | "" :: xs -> aux xs :: extract_board_strings xs
  | _ :: xs -> extract_board_strings xs
  | _ -> []

let calls, boards =
  match lines_of "inputs/input04.txt" with
  | x :: xs ->
      ( String.split_on_char ',' x |> List.map int_of_string
      , extract_board_strings xs
        |> List.map
             (List.map
                (String.split_on_char ' ' >> List.filter (( <> ) "") >> List.map int_of_string) ) )
  | _ -> raise (Failure "shit broke")

module One = struct
  let apply_call n = List.map (List.map (fun x -> if x = n then -1 else x))

  let is_winner board =
    let check_rows = List.exists (fun x -> List.for_all (( = ) (-1)) x) in
    check_rows board || check_rows (Ls.transpose board)

  let rec calculate_winner calls boards =
    match calls, boards with
    | c :: cs, bs -> (
        let post = (List.map << apply_call) c bs in
        match List.find_opt is_winner post with
        | Some board -> board, c
        | _ -> calculate_winner cs post )
    | _ -> raise (Failure "shit broke 2")

  let sum_unmarked = List.fold_left ( + ) 0 << List.filter (( <> ) (-1)) << List.flatten

  let run () =
    let winner, call = calculate_winner calls boards in
    sum_unmarked winner * call |> string_of_int |> print_endline
end

module Two = struct
  let rec calculate_last_winner calls boards =
    match calls, boards with
    | c :: cs, bs ->
        let post = (One.apply_call >> List.map) c bs in
        if List.for_all One.is_winner post then List.find (One.is_winner >> not) bs, c
        else calculate_last_winner cs post
    | _ -> raise (Failure "shit broke 3")

  let run () =
    let loser, call = calculate_last_winner calls boards in
    (One.sum_unmarked loser - call) * call |> string_of_int |> print_endline
end
