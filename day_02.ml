open! Util

module One = struct
  let input =
    lines_of "inputs/input02.txt"
    |> List.map (String.split_on_char ' ' >> fun xs -> List.nth xs 0, List.nth xs 1 |> int_of_string)

  let solve moves =
    let distance, depth =
      List.fold_left
        (fun (distance, depth) (direction, magnitude) ->
          match direction with
          | "up" -> distance, depth - magnitude
          | "down" -> distance, depth + magnitude
          | "forward" -> distance + magnitude, depth
          | _ -> raise (Failure "illegal direction") )
        (0, 0) moves
    in
    distance * depth

  let run () = solve input |> string_of_int |> print_endline
end

module Two = struct
  let solve moves =
    let distance, depth, _ =
      List.fold_left
        (fun (distance, depth, aim) (direction, magnitude) ->
          match direction with
          | "up" -> distance, depth, aim - magnitude
          | "down" -> distance, depth, aim + magnitude
          | "forward" -> distance + magnitude, depth + (aim * magnitude), aim
          | _ -> raise (Failure "illegal direction") )
        (0, 0, 0) moves
    in
    distance * depth

  let run () = solve One.input |> string_of_int |> print_endline
end